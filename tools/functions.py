# This function requires some sort of text input, then asks the user for an email address.
# This is then tested until a legitimately formatted email is inserted
# returns the email address

def get_email(message):
    import re
    email = input(message)
    while not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        print('Invalid email format, please try again\n')
        email = input(message)
    return email


# This functions requires some sort of text input, then ask the user for a status.
# Once one of the 4 legitimate statuses are chosen, that is then returned.

def get_status(message):
    status = input(message)
    while status != "provisioned" and status != "active" and status != "archived" and status != "declined" and status != "":
        print("Invalid input\n")
        status = input(message)
    return status


# This function simply deletes the file called

def del_file(file_name):
    import os
    os.remove(file_name)


# This function ensures that any undecoded data doesn't make it through

def manual_decode(data):
    data = data.replace("\x82","é")
    data = data.replace("\x89","ë")
    data = data.replace("\x93","ô")
    data = data.replace("\x94","ö")
    data = data.replace("\x88","ê")
    data = data.replace("b'","")
    data = data.replace("\r\n'","")
    data = data.lstrip().rstrip()
    return data


def pause():
    from tools.cmd import run_cmd
    run_cmd("pause")


def die():
    from sys import exit
    exit()


def clean_up_lang(data, remove_space = True):
    data = data.replace("1stAddLanguage","").replace("HomeLanguage","").replace("2ndAddLanguage","")
    data = data.replace("1st Add Language","").replace("Home Language","").replace("2nd Add Language","")
    data = data.replace("1staddlanguage","").replace("homelanguage","").replace("2ndaddlanguage","")
    if (remove_space is True):
        data = data.replace("English ","English").replace("Afrikaans ","Afrikaans").replace("French ","French")
    return data


# https://stackoverflow.com/questions/9427163/remove-duplicate-dict-in-list-in-python

def remove_duplicates(data):
    seen = set()
    new_l = []
    for d in data:
        t = tuple(d.items())
        if t not in seen:
            seen.add(t)
            new_l.append(d)
    return new_l


# This check if a file exists

def check_file_exists(file):
    try:
        f = open(file)
        worked = True
        f.close()
    except IOError:
        print('file ' + file + ' does not exist\n')
        worked = False

    return worked


def fix_cn(name):
    """
    Fix a name string that comes in the form of 'surname, first names' and turns it into 'first names surname'

    @param string name

    @return string
    """
    split = name.split(", ")
    return split[1] + " " + split[0]