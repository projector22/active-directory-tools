# Required:
# from cmd import run_cmd

def run_cmd(instruction):
    import os
    os.system(instruction)

# Both work
# os.system('test.bat')
# os.system('ping google.com')

def run_cmd_return(instruction):
    import subprocess
    from tools.functions import manual_decode
    output = subprocess.run(instruction, stdout=subprocess.PIPE, shell=True)

    # the try is because in a few rare cases the decode fails, 
    # this still allows you to return the raw results
    
    try:
        return str(output.stdout.decode('CP437'))
    except UnicodeDecodeError:
        return str(output.stdout)


def run_ps(instruction):
    import subprocess
    x = '"./' + instruction + '"'
    subprocess.call(["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe", ". " + x +";"])