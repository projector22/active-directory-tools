def init_setup():
    # Completed
    # gcds.install_gcds()
    # headings()
    # In Progress
    print("Setup complete")

def headings():
    from tools.functions import pause
    from tools.cmd import run_cmd
    import json
    run_cmd('cls')
    print("When importing CSV user lists, your file needs heading for each category.")
    print("These headings for users are by default:\n")
    print("|===============================|")
    print("|Admin|Grade|Reg|Name|First Name|")
    print("|===============================|")
    print("\nYou can edit what headings the script looks for by editing 'headings.json'")
    print("Note: by changing a heading, you do not change what the app is looking for under that heading")
    pause()    
    print("In the same way, groups lists also need headings for each category.")
    print("These headings for groups are by default:\n")
    print("|========================================|")
    print("|Admin No|Tutor|Subject|Learner|Grade|Set|")
    print("|========================================|")
    print("\nYou can edit what headings the script looks for by editing 'headings.json'")
    print("Note: by changing a heading, you do not change what the app is looking for under that heading")
    pause()
    print("Creating 'heading.json'")
    item1 = {"heading1": "Admin",
            "heading2": "Grade",
            "heading3": "Reg",
            "heading4": "Name",
            "heading5": "First Name"}
    item2 = {"heading1": "Admin No",
            "heading2": "Tutor",
            "heading3": "Subject",
            "heading4": "Learner",
            "heading5": "Grade",
            "heading6": "Set"}

    item = {"users": item1,
            "groups": item2}
    file_name = 'headings.json'

    with open(file_name, 'w') as dump:
        json.dump(item, dump, indent=3)




from lib.gcds import google_AD_sync
gcds = google_AD_sync()
init_setup()