# Active Directory Tool

## Build Requirements

- [x] The program is required to import a CSV and interpret it
- [x] It must create the CSVDE to import users
- [x] It must create the CSVDE to import groups
- [x] It must actually run the CSVDE to do the above
- [x] It must sync the users to Google
- [x] It must set up the GAM commands to create classrooms
- [x] It should ideally check for existing users and update them
- [x] It needs to error check itself
- [ ] It should offer the user automatic syncing on the server or create a batch file which will do the results

## Program Requirements

- [GAM]
- [Google Cloud Directory Sync]
- Administrator access to Active Directory (for CSVDE)

## Prerequisites

- Run `pip install -r requirements.txt`
- Your active directory environment needs to match config.json

## Command Line Options

- `-c` - Loads the config menu and allows the user to modify various configuration options
- `-a` - Load path to 'Student Data'
- `-b` - Load path to 'Class Data'
- `-s` - Run the script silently using default options, works best with -a & -b
- `-o` - Options, works best with -s. If -s is not used, the -o operation will run first, then the menu will be presented
  - `1` - Run a full sync. Default. This is what will run if -s is called but -o is not called.
  - `2` - Run a complete sync, except pause to manually sync Google Cloud Directory Sync via the GUI
  - `3` - Sync Users & Groups to Active Directory
  - `4` - Run Google Cloud Directory Sync
  - `5` - Create and Sync Google Classroom classes from Active Directory

## Credits

- [Excel->CSV]

## Known issues

- While checking if users exist, if a user has a special character in their name such as é or ë etc, the script will perform a dsmove on the user. This moves the user into the folder they started, therefore having no effect.

[GAM]: https://github.com/jay0lee/GAM/
[Excel->CSV]: https://dzone.com/articles/using-python-to-extract-excel-spreadsheet-into-csv
[Google Cloud Directory Sync]: https://tools.google.com/dlpage/dirsync/
