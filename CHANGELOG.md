# CHANGELOG

## 21.02.006

- Added a logo and reduced the screen clearing so a user can go back and view results after a routine has finished and the menu is loaded
- Added userPrincipalName correctly to LDIFDE and fixed displayName

## 21.02.005

- Fixed a bug with group email addresses.
- Fixed a bug where, if the 'both' flag is set, the second group doesn't get memebers
- Fixed a bug where individual class email addresses were not generated accurately

## 21.02.004

- Added a halt for when a config update has been detected from update
- Added an option to allow the user to sync and create groups / classes by subject, or class or both
- Updated the GAM class to use the progress bar and correctly handle the new choices

## 21.02.003

- Added pip requirement `progress` to draw progress bars.
- And with the above in mind, added progress bars rather than per entry feedback for user and group operations. This speeds things up considerably.
- Moved user and group operations over from CSVDE to LDIFDE. Created class `ldifde` for this purpose. Deleted obsolete methods and properties used by CSVDE.
- Added folder `bin` for script generated files to live and `logs` folder for operation logs to live.

## 21.02.002

- Moved the default configuration to `config-default.json`
- Added a class `spreadsheets` for handling the reading of spreadsheet files and reading data from them.
- Added a routine for reading XLSX files as well as CSVs. Cleaned up how both are read. #25
- Replaced `xlrd` with `openpyxl`
- Minor bug fixes

## 21.02.001

- Reorganized and renamed the files into directories, making their names much nicer work with

## 20.02.002

- Added new class `class_config.py` to handle configuration requests and changes
- Consolidated the varsious .json config items into one file `config.json`

## 20.02.001

- Added a simple options menu that presents itself when not running in -s
- Added argument -o to allow the user to parse specific menu choices from the command line.
- Deleted a bunch of old samples and scripts

## 20.01.001

- Added basic arguments: -c (edit config), -a (define file for student sync), -b (define file for group sync), -s (run silently with defaults)
- Added function to `functions.py` to check if a file exists: check_file_exists(file).
- Added function to `functions.py` to kill the script immediately die()

## 18.11.002

- Added a functioning routine to call and run Google Cloud Directory Sync (gcds), syncing users and groups etc to the Google Cloud Directory - 2018-11-06

## 18.11.001

- Built the basic, functioning routines which allow the user to sync users and groups from supplied CSV files to their Active Directory - 2018-11-05
