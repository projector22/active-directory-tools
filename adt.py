# This script is meant to function as the main script called when a sync is initiated

import argparse
from tools.functions import die, check_file_exists, pause
from lib.config import config
config = config()

def draw_logo():
    print("""

 .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. |
| |      __      | || |  ________    | || |  _________   | |
| |     /  \     | || | |_   ___ `.  | || | |  _   _  |  | |
| |    / /\ \    | || |   | |   `. \ | || | |_/ | | \_|  | |
| |   / ____ \   | || |   | |    | | | || |     | |      | |
| | _/ /    \ \_ | || |  _| |___.' / | || |    _| |_     | |
| ||____|  |____|| || | |________.'  | || |   |_____|    | |
| |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------' 
 
 """)

# The actions to sync to sync to the active directory components

def sync_ad(silent = False):
    if (silent == False):
        print("Checking all Organisational Units are in place\n")
    run_ad.check_and_create_ous()

    if (silent == False):
        print("Syncing Users\n")
    run_ad.append_users(file_users)

    if (silent == False):
        print("Syncing Groups\n")
    run_ad.append_groups(file_groups)

    if (silent == False):
        print("\n\nAll user and group actions complete")

# The actions to sync to Google Cloud Directory Sync components

def sync_gcds(silent = False):
    if (silent == False):
        print("Syncing the AD to Google\n")
        try:
            gcds.sync_to_google()
        except FileNotFoundError:
            import sys
            sys.exit('Cannot find the json file, perhaps run the install config again')
            # TODO: Check how to word this
    else:
        try:
            gcds.sync_to_google()
        except FileNotFoundError:
            print("Skipping Google Cloud Directory Sync - This is recommended before syncing to Google Classroom")

# The actions to sync to Google Classroom components

def sync_gc(silent = False):
    if (silent == False):
        print("Step 6:\n Syncing to Google Classroom")
    gam.user_info()

# Handle cases where an -o argument is presents

def handle_option_argument_actions(arg, silent):
    if (arg != None):
        if arg == '1':
            sync_ad(silent)
            sync_gcds(silent)
            sync_gc(silent)
        elif arg == '2':
            sync_ad(silent)
            print('Sync to Google via the Google Cloud Directory Sync app now')
            pause()
            sync_gc(silent)
        elif arg == '3':
            sync_ad(silent)
        elif arg == '4':
            sync_gcds(silent)
        elif arg == '5':
            sync_gc(silent)

ap = argparse.ArgumentParser()
ap.add_argument("-c", required=False, action='store_true', help="Edit config options")
ap.add_argument("-a", required=False, help="Load path to 'Student Data'")
ap.add_argument("-b", required=False, help="Load path to 'Class Data'")
ap.add_argument("-s", required=False, action='store_true', help="Run the script silently using default options, works best with -a & -b")
ap.add_argument("-o", required=False, help="Options - works best with -s. For complete list of options see 'readme.md'. If -s is not used, the -o operation will run first, then the menu will be presented")
arg = vars(ap.parse_args())

if (arg['c'] == True):
    print('Modifying config')
    config.menu()
    die()

path_a = None
path_b = None

# Check if the 'a' argument has been parsed
if (arg['a'] != None):
    path_a = arg['a']
    if (check_file_exists(path_a) == False):
        die()

# Check if the 'b' argument has been parsed
if (arg['b'] != None):
    path_b = arg['b']
    if (check_file_exists(path_b) == False):
        die()

# Check if the 's' argument has been parsed
silent = False
if (arg['s'] == True):
    silent = True

# Check if the config exists
if (config.check_config_exists() == False):
    print("Config created and execution halted. Please run 'active-directory-tools.py -c' to modify settings as desired before running sync")
    die()

from lib.active_directory import active_directory
from lib.gcds import google_AD_sync
from lib.gam import gam
from tools.cmd import run_cmd

run_ad = active_directory()
gcds = google_AD_sync()
gam = gam()

run_cmd("@ECHO off")
run_cmd("cls")

if (silent == False and path_a == None and path_b == None):
    print("Adding the CSV files to sync")

if (path_a == None):
    file_set = False
    while (file_set == False):
        file_users = input("  Enter the name of the file with user information you wish to sync:\n->")
        file_set = check_file_exists(file_users)
else:
    file_users = path_a

if (path_b == None):
    file_set = False
    while (file_set == False):
        file_groups = input("  Enter the name of the file with group information you wish to sync:\n->")
        file_set = check_file_exists(file_groups)
else:
    file_groups = path_b

if (silent == False):
    handle_option_argument_actions(arg['o'], silent)
    run_cmd("cls")
    while(True):
        run_cmd("@ECHO off")
        draw_logo()
        print("\t***OPTIONS***\n") 
        print("1.\tFull Sync")
        print("2.\tFull Sync, pausing for manual Google Cloud Directory Sync")
        print("3.\tSync Users & Groups to Active Directory")
        print("4.\tRun Google Cloud Directory Sync")
        print("5.\tCreate and Sync Google Classroom classes from Active Directory")
        print("6.\tQuit\n")
        choice = input("Enter an option 1-6 and press enter:\t")
        if (choice == '1'):
            sync_ad()
            sync_gcds()
            sync_gc()
        elif (choice == '2'):
            sync_ad()
            print('Sync to Google via the Google Cloud Directory Sync app now')
            pause()
            sync_gc()
        elif (choice == '3'):
            sync_ad()
        elif (choice == '4'):
            sync_gcds()
        elif (choice == '5'):
            sync_gc()
        elif (choice == '6'):
            break
        else:
            print('Invalid input')
            pause()
else:
    if (arg['o'] != None):
        handle_option_argument_actions(arg['o'], silent)
    else:
        sync_ad(True)
        sync_gcds(True)
        sync_gc(True)

run_cmd("@ECHO on")