class active_directory():

    def __init__(self):
        self.config = 'config.json'

        self.q = '"'
        self.c = ','
        self.s = ' '
        self.d = '-'
        self.sds = ' - '

        import time
        self.this_year = time.strftime("%Y")

        from lib.config import config
        config = config()
        self.config_headings = config.load_config_data()['headings']
        # By default: "OU=Students,DC=example,DC=com"
        self.student_context = self.c + config.load_config_data()['ad']['parent_ou']
        # By default: "OU=Classroom Distribution,OU=Students,DC=example,DC=com"
        self.group_context = self.c + config.load_config_data()['ad']['groups'] + self.student_context
        self.school = config.load_config_data()['ad']['company']
        self.class_generation_options = config.load_config_data()['class_generation_options']
        self.subject_options = config.load_config_data()['subject_options']
        
        self.addomain = "@" + config.load_config_data()['ad']['domain'] # The domain of the AD
        self.maildom = "@" + config.load_config_data()['gam']['domain'] # This is the email address assigned on the AD to the user. For example 1234@example.com


    def read_spreadsheet(self, file_name, data_type):
        """
        This method reads a spreadsheet (xlsx or csv) file and processes the data, returning everything in a usuable format

        @param Type: string Name: file_name - The name of the CSV file to be processed
        
        @param Type: string Name data_type - The type of data being interpreted, 'user' or 'group'

        @return list
        """

        from lib.spreadsheets import spreadsheets
        spreadsheet = spreadsheets()
        spreadsheet.context = data_type
        spreadsheet.file = file_name
        try:
            spreadsheet.get_file_data()
        except NameError:
            print("Error: " + file_name + " isn't a compatible file type")
            from tools.functions import die
            die()
        return spreadsheet.data


    def append_users(self, file_name):
        """
        This method gets user data from the inputed spreadsheet and executes it through CSVDE for users.
        it checks for existing users and does not sync them, it also moves any out of place users

        @param Type: string Name file_name - Name of the file being edited
        """
        from tools.functions import del_file
        from tools.cmd import run_cmd, run_cmd_return
        from tools.functions import manual_decode, die
        from lib.ldifde import ldifde
        import os.path
        from progress.bar import Bar

        new_users = ldifde()

        new_users.file = os.path.abspath(os.path.dirname(os.path.abspath(__file__))) + '\\..\\bin\\' + "users.ldf"

        if os.path.exists(new_users.file) == True:
            del_file(new_users.file) # Delete the file if it already exists

        users = dict()
        data = self.read_spreadsheet(file_name, 'users')
        with Bar('Processing', max=len(data)) as bar:
            for item in data:
                grade_ou = "Matric" + self.get_student_matric_year(item['Grade'])
                ou = ',OU=' + grade_ou + self.student_context
                dn = 'CN=' + item['First Name'] + self.s + item['Name'] + ou

                if int(item['Grade']) < 10:
                    item['Grade'] = "0" + str(item['Grade'])

                description = 'Grade ' + item['Grade']
                sAMAccountname = item['Admin']
                sn = item['Name']
                givenName = item['First Name']
                mail = sAMAccountname + self.maildom
                upn = sAMAccountname + self.addomain

                test_lookup = manual_decode(run_cmd_return("dsquery user domainroot -samid " + sAMAccountname))
                test_dn = self.q + 'CN=' + givenName + self.s + sn + ',OU=' + grade_ou + self.student_context + self.q
                if test_lookup == '':
                    if sAMAccountname not in users:
                        users[sAMAccountname] = {
                            'dn': dn,
                            'sn': sn,
                            'givenName': givenName,
                            'displayName': givenName + self.s + sn,
                            'mail': mail,
                            'description': description,
                            'sAMAccountname': sAMAccountname,
                            'upn': upn,
                            'company': self.school,
                            'department': "Grade " + item['Grade'],
                            'title': "Learner"
                        }
                elif test_dn != test_lookup:
                    # test_dn - what it should be
                    # test_lookup - what it currently is
                    new_name = self.q + givenName + " " + sn + self.q
                    new_loc = self.q + "OU=" + grade_ou + self.student_context + self.q
                    move = "dsmove " + test_lookup + " -newparent " + new_loc + " -newname " + new_name
                    print("\n")
                    run_cmd(move)
                bar.next()

        if len(users) > 0:
            new_users.prepare_user_text(users)
            new_users.execute_ldifde()


    def append_groups(self, file_name):
        """
        This method gets the sorted data from the inputed spreadsheet and executes it through CSVDE for groups
        
        @param Type: string Name file_name - Name of the file being edited
        """
        from tools.functions import del_file, clean_up_lang, remove_duplicates, fix_cn, die
        from tools.cmd import run_cmd_return, run_cmd
        from lib.ldifde import ldifde
        import os.path

        create_groups = ldifde()
        group_members = ldifde()

        create_groups.file = os.path.abspath(os.path.dirname(os.path.abspath(__file__))) + '\\..\\bin\\' + "groups.ldf"
        group_members.file = os.path.abspath(os.path.dirname(os.path.abspath(__file__))) + '\\..\\bin\\' + "members.ldf"

        if os.path.exists(create_groups.file) == True:
            del_file(create_groups.file) # Delete the file if it already exists

        if os.path.exists(group_members.file) == True:
            del_file(group_members.file) # Delete the file if it already exists
        
        data = remove_duplicates(self.read_spreadsheet(file_name,'groups'))

        group_membership = dict()
        groups = dict()

        for item in data:
            name = fix_cn(item['Learner'])
            grade_ou = "OU=Matric" + self.get_student_matric_year(item['Grade'])
            learner_cn = "CN=" + name + self.c + grade_ou + self.student_context
            if item['Grade'] == "8" or item['Grade'] == "9" or item['Grade'] == "9" :
                item['Grade'] = '0' + item['Grade']

            item['Subject'] = clean_up_lang(item['Subject']).strip()

            if item['Subject'] in self.subject_options:
                if self.subject_options[item['Subject']] in self.class_generation_options:
                    group_name = self.prepare_group_name(self.subject_options[item['Subject']], item)
                    group_samId = self.prepare_samID(self.subject_options[item['Subject']], item)
                    description = self.prepare_group_description(self.subject_options[item['Subject']], item)
                    mail = self.prepare_group_email(self.subject_options[item['Subject']], item)
                else:
                    group_name = self.prepare_group_name(self.subject_options['default'], item)
                    group_samId = self.prepare_samID(self.subject_options['default'], item)
                    description = self.prepare_group_description(self.subject_options['default'], item)
                    mail = self.prepare_group_email(self.subject_options['default'], item)
            else:
                group_name = self.prepare_group_name(self.subject_options['default'], item)
                group_samId = self.prepare_samID(self.subject_options['default'], item)
                description = self.prepare_group_description(self.subject_options['default'], item)
                mail = self.prepare_group_email(self.subject_options['default'], item)

            # If the "both" context is set, the data will be in the form of a list
            if type(group_name) is list:
                for i in range(len(group_name)):
                    group_DN = "CN=" + group_name[i] + self.group_context
                    if group_samId[i] not in groups:
                        groups[group_samId[i]] = {
                            "DN": group_DN,
                            "CN": group_name[i],
                            "description": description[i],
                            "sAMAccountName": group_samId[i],
                            "mail": mail[i]
                        }
                    if group_DN not in group_membership:
                        group_membership[group_DN] = list()
                    group_membership[group_DN].append(learner_cn)
            else:
                group_DN = "CN=" + group_name + self.group_context
                if group_samId not in groups:
                    groups[group_samId] = {
                        "DN": group_DN,
                        "CN": group_name,
                        "description": description,
                        "sAMAccountName": group_samId,
                        "mail": mail
                    }
                if group_DN not in group_membership:
                    group_membership[group_DN] = list()
                group_membership[group_DN].append(learner_cn)

        create_groups.prepare_group_creation_text(groups)
        create_groups.execute_ldifde()
        
        group_members.prepare_group_members_text(group_membership)
        group_members.execute_ldifde()


    def prepare_group_name(self, context, item):
        """
        Set the group name, based on context

        @param string context - The context of group name

        @param dictionary item - Informatation from which to form the return

        @return string / list
        """
        if context == "s":
            return item['Subject'] + self.sds + "Grade" + self.s + item['Grade'] + self.sds + self.this_year
        elif context == "c":
            return item['Subject'] + self.sds + item['Tutor'] + self.sds + item['Set'] + self.sds + self.this_year
        elif context == "b":
            return [
                self.prepare_group_name("s", item),
                self.prepare_group_name("c", item)
            ]


    def prepare_samID(self, context, item):
        """
        Set the sAMAccountName, based on context

        @param string context - The context of the sAMAccountName

        @param dictionary item - Informatation from which to form the return

        @return string / list
        """
        if context == "s":
            return item['Subject'].replace(" ", "").lower() + self.d + "grade" + item['Grade'] + self.d + self.this_year
        elif context == "c":
            return item['Subject'].replace(" ", "").lower() + self.d + item['Tutor'].replace(" ", "").lower() + self.d + "grade" + item['Grade'] + self.d + item['Set'] + self.d + self.this_year
        elif context == "b":
            return [
                self.prepare_samID("s", item),
                self.prepare_samID("c", item)
            ]


    def prepare_group_description(self, context, item):
        """
        Set the group description, based on context

        @param string context - The context of the description

        @param dictionary item - Informatation from which to form the return

        @return string / list
        """
        if context == "s":
            return "Grade" + self.s + item['Grade'] + self.sds + item['Subject'] + self.sds + self.this_year + self.s + "Distribution Group"
        elif context == "c":
            return "Grade" + self.s + item['Grade'] + self.sds + item['Subject'] + self.sds + item['Tutor'] + self.sds + item['Set'] + self.sds + self.this_year + self.s + "Distribution Group"
        elif context == "b":
            return [
                self.prepare_group_description("s", item),
                self.prepare_group_description("c", item)
            ]


    def prepare_group_email(self, context, item):
        """
        Set the group email address, based on context

        @param string context - The context of the description

        @param dictionary item - Informatation from which to form the return

        @return string / list
        """
        if context == "s":
            return item['Subject'].replace(" ", "").lower() + self.d + "grade" + item['Grade'] + self.d + self.this_year + self.maildom
        elif context == "c":
            return item['Subject'].replace(" ", "").lower() + self.d + item['Tutor'].replace(" ", "").lower() + self.d + item['Set'].replace(" ", "").lower() + self.d + self.this_year + self.maildom
        elif context == "b":
            return [
                self.prepare_group_email("s", item),
                self.prepare_group_email("c", item)
            ]


    def check_and_create_ous(self):
        """
        This method checks if the currently required organisational units are present and complete and if not
        it creates the current requirements based on the year
        """

        from progress.bar import Bar
        years = [
            'Matric' + self.this_year,               # Grade 12
            'Matric' + str(int(self.this_year) + 1), # Grade 11
            'Matric' + str(int(self.this_year) + 2), # Grade 10
            'Matric' + str(int(self.this_year) + 3), # Grade 09
            'Matric' + str(int(self.this_year) + 4)  # Grade 08
        ]
        print("Checking required Organizational Unit structure\n")
        with Bar('Processing', max=len(years)+2) as bar:
            dn = self.q + self.student_context[1:] + self.q
            self.check_and_create_ou(dn)
            bar.next()
            dn = self.q + self.group_context[1:] + self.q
            self.check_and_create_ou(dn)
            bar.next()

            for year in years:
                dn = '"OU=' + year + self.student_context + self.q
                self.check_and_create_ou(dn)
                bar.next()


    def check_and_create_ou(self, dn):
        """
        Performs a dsquery ou instruction to test the existance of an OU and creates it if it doesn't exist
        """
        from tools.cmd import run_cmd, run_cmd_return
        instruction = 'dsquery ou ' + dn
        result = run_cmd_return(instruction)
        if 'dsquery failed' in result or result == '':
            print(self.s)
            run_cmd("dsadd ou " + dn)


    def get_student_matric_year(self, grade):
        """
        Get the the year the learner will be leaving the school, given the parsed grade number

        @param  number  grade   The grade of the learner

        @return string  The year the learner will leave, formatted as a string
        """
        if grade == "12":
            year = int(self.this_year) + 0
        elif grade == "11":
            year = int(self.this_year) + 1
        elif grade == "10":
            year = int(self.this_year) + 2
        elif grade == "9":
            year = int(self.this_year) + 3
        elif grade == "8":
            year = int(self.this_year) + 4
        return str(year)
