class ldifde:

    def __init__(self):
        self.file = None
        self.text = None
        self.br = "\n"

    
    def write_to_file(self, meth='a'):
        """
        This method appends entries to self.file file being prepared for syncing

        @param string meth -> Optional, the method being used. Default='a' optionally 'w'
        """
        with open(self.file, meth) as file_object:
            file_object.write(self.text)
        

    def prepare_group_creation_text(self, data):
        """
        Prepare and append ldifde data to create groups

        @param dictionary data
        """
        print("\nPreparing groups")
        from progress.bar import Bar
        self.text = ''
        with Bar('Processing', max=len(data)) as bar:
            for sam_id in data:
                elements = data[sam_id]
                self.text += "DN: " + elements['DN'] + self.br
                self.text += "changeType: add" + self.br
                self.text += "CN: " + elements['CN'] + self.br
                self.text += "description: " + elements['description'] + self.br
                self.text += "mail: " + elements['mail'] + self.br
                self.text += "objectClass: group" + self.br
                self.text += "sAMAccountName: " + elements['sAMAccountName'] + self.br + self.br
                bar.next()
        self.write_to_file()


    def prepare_group_members_text(self, data):
        """
        Prepare and append ldifde data to add members to groups

        @param dictionary data
        """
        print("\nPreparing group members")
        from progress.bar import Bar
        self.text = ''
        with Bar('Processing', max=len(data)) as bar:
            for entry in data:
                for member in list(dict.fromkeys(data[entry])): # Remove any duplicate values
                    self.text += "DN: " + entry + self.br
                    self.text += "changeType: modify" + self.br
                    self.text += "add: member" + self.br
                    self.text += "member: " + member + self.br
                    self.text += '-' + self.br + self.br
                bar.next()
        self.write_to_file()


    def prepare_user_text(self, data):
        """
        Prepare and append ldifde data to add user accounts

        @param dictionary data
        """
        print("\nPreparing user accounts")
        from progress.bar import Bar
        self.text = ''
        with Bar('Processing', max=len(data)) as bar:
            for sam_id in data:
                elements = data[sam_id]
                self.text += "DN: " + elements['dn'] + self.br
                self.text += "changeType: add" + self.br
                self.text += "CN: " + elements['givenName'] + " " + elements['sn'] + self.br
                self.text += "objectClass: user" + self.br
                self.text += "sAMAccountname: " + elements['sAMAccountname'] + self.br
                self.text += "displayName: " + elements['displayName'] + self.br
                self.text += "userPrincipalName: " + elements['upn'] + self.br
                self.text += "givenName: " + elements['givenName'] + self.br
                self.text += "sn: " + elements['sn'] + self.br
                self.text += "description: " + elements['description'] + self.br
                self.text += "useraccountControl: 544" + self.br
                self.text += "company: " + elements['company'] + self.br
                self.text += "department: " + elements['department'] + self.br
                self.text += "title: " + elements['title'] + self.br
                self.text += "mail: " + elements['mail'] + self.br + self.br
                bar.next()
        self.write_to_file()


    def execute_ldifde(self):
        """
        Execute an ldifde import
        """
        from tools.cmd import run_cmd
        run_cmd("ldifde -i -f " + self.file + " -j .\logs -k")