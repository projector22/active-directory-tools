class spreadsheets():
    
    def __init__(self):
        self.context = None
        self.file = None
        self.data = list()
        from lib.config import config
        config = config()
        self.config_headings = config.load_config_data()['headings']


    def get_file_data(self):
        """
        Read file in it's correct format and return appropriately sorted data
        """
        if self.file.find(".xlsx") != -1:
            self.read_xlsx()
        elif self.file.find(".csv") != -1:
            self.read_csv()
        else:
            raise NameError


    def read_xlsx(self):
        """
        Read the data from the designated XLSX file into self.data
        """
        from sys import exit
        sheet_headings = list()
        from openpyxl import load_workbook
        try:
            wb = load_workbook(self.file)
            sheet = wb.worksheets[0]
        except:
            exit("Unable to read " + self.file)
        for heading in sheet[1]:
            sheet_headings.append(heading.value)
        self.test_headings(sheet_headings)
        headings = list()
        for pos in self.config_headings[self.context]:
            headings.append(self.config_headings[self.context][pos])
        i = 0
        for row in sheet.iter_rows():
            if i == 0:
                i = 1
                continue # skip the title row
            hold = dict()
            for heading in headings:
                hold[heading] = str(row[sheet_headings.index(heading)].value)
            self.data.append(hold)
        

    def read_csv(self):
        """
        Read the data from the designated CSV file into self.data
        """
        import csv
        headings = list()
        for pos in self.config_headings[self.context]:
            headings.append(self.config_headings[self.context][pos])
        with open(self.file,'r', encoding='ISO-8859-1') as content:
            i = 0
            for row in content:
                if i == 0:
                    i = 1
                    sheet_headings = row.replace("\n","").split(",")
                    self.test_headings(sheet_headings)
                    continue # skip the title row
                hold = dict()
                data = next(csv.reader(row.splitlines(), skipinitialspace = True))
                for heading in headings:
                    hold[heading] = str(data[sheet_headings.index(heading)])
                self.data.append(hold)


    def test_headings(self, sheet_headings):
        """
        Test if the headings from the spreadsheet match the config and are all present
        """
        for item in self.config_headings[self.context]:
            if self.config_headings[self.context][item] not in sheet_headings:
                print("Error: The required heading '" + self.config_headings[self.context][item] + 
                "' is not in your spreadsheet " + self.file + ". The application cannot continue...")
                from sys import exit
                exit()


    def append_to_csv(self, data, meth='a'):
        """
        This method appends entries to self.file CSV file being prepared for syncing
        
        @param string data -> the data to be appended to/ written to the file
        
        @param string meth -> Optional, the method being used. Default='a' optionally 'w'
        """
        with open(self.file, meth) as file_object:
            file_object.write(data)
