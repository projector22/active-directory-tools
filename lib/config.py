class config():

    def __init__(self):
        self.config = "config.json"
        self.default_config = "config-default.json"
        self.config_updated = False


    def write_changes_to_json(self, data):
        """
        Write any changes permenetly to the config file

        @param  Type: list    Name: data    The data to be written
        """
        import json
        with open(self.config, 'w') as config_file:  
            json.dump(data, config_file, indent=3)


    def load_config_data(self, config = None):
        """
        Load the config data from config.json and return data

        @param Type: JSON Name: config - The default config data loaded is self.config

        @return list
        """
        if config == None:
            config = self.config
        import json
        with open(config) as item:
            return json.load(item)


    def check_config_exists(self):
        """
        Test if config.json file exists, and tests if it is up to date

        @return bool
        """
        try:
            open(self.config, 'r')
            default_config = self.load_config_data(self.default_config)
            config = self.load_config_data()
            self.write_changes_to_json(self.check_config_values(default_config, config))
            if self.config_updated == True:
                from sys import exit
                exit("Config has been updated. Please check config.json and restart ADT")
            return True
        except FileNotFoundError:
            print("Config missing, loading Defaults...")
            self.write_default_config()
            return False


    def write_default_config(self):
        """
        Copy the file, config-default.json to config.json which the app would use.
        Note, to update the default config, you must edit config-default.json
        """
        from shutil import copyfile
        copyfile(self.default_config, self.config)


    def check_config_values(self, default, live):
        """
        Check the default config against the live config, 
        add any new fields to live without changing existing values.

        @param  Type: dict, Name: default   The default config
        @param  Type: dict, Name: live      The live config

        @return dict
        """
        for val in default:
            if val not in live:
                live[val] = default[val]
                self.config_updated = True
            else:
                if type(default[val]) is dict:
                    live[val] = self.check_config_values(default[val], live[val])
        
        return live


    # The menu that runs when calling -c
    
    def menu(self):
        from tools.cmd import run_cmd
        self.check_config_exists()
        
        while(True):
            run_cmd("@ECHO off")
            run_cmd("cls")
            print("\t***OPTIONS***\n") 
            print("1.\tModify Headings")
            print("2.\tModify GCDS Data")
            print("3.\tReset config to default")
            print("4.\tQuit")

            choice = input("Enter an option 1-4 and press enter:\t")
            if (choice == '1'):
                self.modify_headings()
            if (choice == '2'):
                self.modify_gcds_options()
            elif (choice == '3'):
                self.write_default_config()
            elif (choice == '4'):
                break


    # Goes through the action of updating and writing to the Headings section of config.json

    def modify_headings(self):
        from tools.functions import pause
        config = self.load_config_data()

        for c in range(1,5):
            i = str(c)
            print("User Heading " + i + " is " + config['headings']['users']['heading' + i] + " - Press e to edit this value, any other key to continue to the next heading")
            config_change = input()
            if (config_change == 'e'):
                new_val = input("Enter A new value: ")
                config['headings']['users']['heading' + i] = new_val

        for c in range(1,6):
            i = str(c)
            print("Groups Heading " + i + " is " + config['headings']['groups']['heading' + i] + " - Press e to edit this value, any other key to continue to the next heading")
            config_change = input()
            if (config_change == 'e'):
                new_val = input("Enter A new value: ")
                config['headings']['groups']['heading' + i] = new_val

        self.write_changes_to_json(config)
        print("All modifications complete")
        pause()

    # Goes through the action of updating and writing to the gcds section of config.json
    
    def modify_gcds_options(self):
        from tools.functions import pause
        config = self.load_config_data()

        print("sync-cmd.exe is located at " + config['gcds']['sync_cmd'] + " - Press e to edit this value, any other key to continue to the next item")
        config_change = input()
        if (config_change == 'e'):
            new_val = input("Enter A new value: ")
            config['gcds']['sync_cmd'] = new_val

        print("The gcds config xml is located at " + config['gcds']['data_xml'] + " - Press e to edit this value, any other key to continue to the next item")
        config_change = input()
        if (config_change == 'e'):
            new_val = input("Enter A new value: ")
            config['gcds']['data_xml'] = new_val

        self.write_changes_to_json(config)
        print("All modifications complete")
        pause()
