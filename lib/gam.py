class gam():
    
    def __init__(self):
        self.print = "gam print courses "
        self.create = "gam create course "
        self.update = "gam update course "
        self.course = "gam course "
        self.q = '"'
        self.c = ','

        from lib.config import config
        config = config()

        self.default_teacher = config.load_config_data()['gam']['default_teacher']
        self.school_domain = "@" + config.load_config_data()['gam']['domain']
        # By default: "OU=Students,DC=example,DC=com"
        self.student_context = self.c + config.load_config_data()['ad']['parent_ou']
        # By default: "OU=Classroom Distribution,OU=Students,DC=example,DC=com"
        self.group_context = config.load_config_data()['ad']['groups'] + self.student_context

        import os.path
        path = os.path.abspath(os.path.dirname(os.path.abspath(__file__))) + '\\..\\bin\\'
        self.sync_file = path + 'sync_classroom_students.bat'
        self.sync_file_teachers = path + 'sync_classroom_teachers.bat'


    def user_info(self):
        """
        This method collects the information required to execute the gam
        It runs a "Print all courses" command and then examines the results
        to determine what is needed and what isn't
        It then generates the classrooms and creates & runs the batch script to sync the students, teachers etc.
        """
        from tools.cmd import run_cmd_return, run_cmd
        from tools.functions import manual_decode, del_file, clean_up_lang, die
        from progress.bar import Bar
        try:
            del_file(self.sync_file)
            self.create_batch_file(self.sync_file)
        except FileNotFoundError:
            pass
        try:
            del_file(self.sync_file_teachers)
            self.create_batch_file(self.sync_file_teachers)
        except FileNotFoundError:
            pass
        courses = list()
        g_results = manual_decode(run_cmd_return("gam print courses state active")).split('\n')
        for items in g_results:
            item = items.split(',')
            courses.append({"id": item[0],
                            "name": item[1],
                            "alias": item[1].replace(" ","").replace('&','and').lower()})

        results = run_cmd_return("dsquery group " + self.q + self.group_context + self.q + " -limit 1000").split('\n')

        found_courses = 0
        new_courses = 0
        with Bar('Processing', max=len(results)) as bar:
            for result in results:
                if result == "":
                    bar.next()
                    continue
                match = False
                name = clean_up_lang(result.split(',')[0].replace('"CN=',''), False) # The name of the group if a new group is needed
                test = name.replace(" ", "").replace('&','and').lower() # The alias of the group to be compared to the list of existing groups
                group_email = test + self.school_domain
                teacher_email = '????????' + self.school_domain
                # print("\nMatching " + test) # Uncomment for more verbose debugging
                for item in courses:
                    if item['alias'] == test:
                        match = True
                        break
                if match == False:
                    new_courses += 1
                    # print("No match found, creating new course") # Uncomment for more verbose debugging
                    from tools.functions import pause
                    alias = test.replace('&','and')
                    teacher = self.default_teacher
                    self.create_course(alias, name, teacher)
                    self.create_gam_sync_script(alias, self.sync_file, group_email)
                    self.create_gam_sync_script(alias, self.sync_file_teachers,teacher_email, 'teachers')
                elif match == True:
                    found_courses += 1
                    # print('Match Found') # Uncomment for more verbose debugging
                    send = test.replace('&','and')
                    self.create_gam_sync_script(send, self.sync_file, group_email)
                    self.create_gam_sync_script(send, self.sync_file_teachers, teacher_email, 'teachers')
                bar.next()
        print("\nExisting courses: " + str(found_courses))
        print("New courses: " + str(new_courses))
        print("\nCourse creation complete... now adding students")
        run_cmd(self.sync_file)
        print("The program has generated the following files\n\n")
        print("\t" + self.sync_file + "\n\t" + self.sync_file_teachers + "\t<- This file needs to be modified with your own teacher groups")


    def create_gam_sync_script(self, alias, file_name, group, user = 'students', method = 'a'):
        """
        This method appends a gam sync instruction to a constructed batch file

        `@param string` alias

        `@param string` file_name

        `@param string` group
        
        `@param string` user -> default = 'students'

        `@param string` method -> default = 'a'
        """
        instruction = self.course + alias + " sync " + user + " group " + group
        with open(file_name, method) as batch_file:
            batch_file.write(instruction + "\n")


    def create_batch_file(self, file_path):
        """
        The purpose of this method is to generate a batch script with the required lines, especially `@echo off`
        """
        with open(file_path, "w") as batch_file:
            batch_file.write("@echo off\n::This file is generated by ADT\n\n")


    def user_info_todrive(self, email, state=''):
        """
        This method gets takes inputs and pipes them through as a gam command to cmd
        
        `@param string` email -> "teacher teacher_id@example.com" or "student student_id@example.com"

        `@param string` state -> "state active" or "state archived" or "state active,provisioned" or left blank
        """
        from tools.cmd import run_cmd
        instruction = self.print + state + " " + email + " todrive"
        run_cmd(instruction)


    def get_user_info(self):
        """
        This method is called if the user wants to get info out of the domain.
        It asks for inputs and pipes the instruction to user_info
        """
        from tools.functions import get_email

        def test_status(tested_input):
            legit = False
            if tested_input != '' or tested_input != 'active' or tested_input != 'provisioned' or tested_input != 'archived' or tested_input != 'active,provisioned' or tested_input != 'active,archived' or tested_input != 'provisioned,active' or tested_input != 'provisioned,archived' or tested_input != 'archived,active' or tested_input != 'archived,provisioned':
                legit = True
            return legit
        message = "Enter the desired email address:\n"
        email = get_email(message)

        desired_state = input("Enter a desired state (active,provisioned,archived). Leave blank to see all states:\n")
        while test_status(desired_state) == False:
            print('Invalid input, please try again\n')
            desired_state = input("Enter a desired state (active,provisioned,archived). Leave blank to see all states:\n")
                
        user = "teacher " + email
        if desired_state != '' and test_status(desired_state):
            state = "state " + desired_state
            self.user_info_todrive(user,state)
        else:
            self.user_info_todrive(user)


    def create_course(self, alias, name, teacher, status='active', section='', heading='', description='', room=''):
        """
        This method takes user input and executes a gam to cmd to create a Google Classroom Class

        `@param string` alias -> expected format subject-grade-year ... no spaces allowed
        
        `@param string` name -> expected format Subject - Grade - Year
        
        `@param string` teacher -> teacher's email

        `@param string` status -> default "active", options <PROVISIONED|ACTIVE|ARCHIVED|DECLINED>

        `@param string` section -> optional element, type STRING

        `@param string` heading -> optional element, type STRING

        `@param string` description -> optional element, type STRING

        `@param string` room -> optional element, type STRING

        minimum input: create_course(alias, name, teacher)
        """
        from tools.cmd import run_cmd
        instruction = self.create
        instruction += "alias " + alias + " name " + self.q + name.title() + self.q
        if section != '':
            instruction += " section " + self.q + section + self.q
        if heading != '':
            instruction += " heading " + self.q + heading.title() + self.q
        if description != '':
            instruction += " description " + self.q + description + self.q
        if room != '':
            instruction += " room " + self.q + room.title() + self.q
        instruction += " teacher " + teacher + " status " + status
        print(" Creating course '" + name + "' ... ")
        run_cmd(instruction)


    def user_create_course(self):
        """
        This method is called if the user wants create a single Google Classroom class
        it asks for inputs and pipes the instruction to create_course
        """
        from tools.functions import get_email, get_status

        name = input("Enter a name for your classroom\nExpected format Subject - Grade - Year:\n")
        alias = name.lower().replace(" ", "")
        
        get_mail_msg = "Enter email address of the owner/teacher for your classroom:\n"
        teacher = get_email(get_mail_msg)
        
        message = "Optional: Set a status\nOptions are: provisioned | active | archived | declined\nDefault action if left blank - active\n"
        status = get_status(message)
        
        section = input("Optional: Set a 'section' for your Classroom\nLeave blank to pass this section\n")
        heading = input("Optional: Set a 'heading' for your Classroom\nLeave blank to pass this section\n")
        description = input("Optional: Set a 'description' for your Classroom\nLeave blank to pass this section\n")
        room = input("Optional: Set a 'room' for your Classroom\nLeave blank to pass this section\n")

        if room != '':
            self.create_course(alias, name, teacher, status, section, heading, description, room)
        elif description != '':
            self.create_course(alias, name, teacher, status, section, heading, description)
        elif heading != '':
            self.create_course(alias, name, teacher, status, section, heading)
        elif section != '':
            self.create_course(alias, name, teacher, status, section)
        elif section != '':
            self.create_course(alias, name, teacher, status)
        else:
            self.create_course(alias, name, teacher)


    def update_course(self, alias, name = '', status = '', owner = ''):
        """
        This method takes user input and executes a gam to cmd to update a Google Classroom class
        
        `@param string` alias -> expected format subject-grade-year or class id, usually a string of numbers
        
        `@param string` name -> optional element, use to update the course's name
        
        `@param string` status -> optional element, use to update the course's status
        
        `@param string` owner -> optional element, use to update the course's owner
        """
        from tools.cmd import run_cmd
        instruction = self.update + alias
        if name != '':
            instruction += " name " + self.q + name + self.q
        if status != '':
            instruction += " status " + status
        if owner != '':
            instruction += " owner " + owner
        print("Attempting to update course")
        run_cmd(instruction)


    def user_update_course(self):
        """
        This method is called if the user wants to update a single Google Classroom class
        it asks for inputs and pipes the instructions to update_course
        """
        from tools.functions import get_email, get_status
        pass_on = "Press enter to skip this category\n"
        alias = input('Enter the ID or alias of the course you wish to update\n')
        name = input('If you wish to change the name of the course, type it in here\n' + pass_on)
        
        status_message = 'If you wish to update the status of the course, type it here\nChoices: provisioned | active | archived | declined\n' + pass_on
        status = get_status(status_message)
        
        owner_message = 'If you wish to change the owner of this course, type it in here\n' + pass_on
        owner = get_email(owner_message)
        
        if name != '' and status != '' and owner != '':
            self.update_course(alias,name,status,owner)
        else:
            print('Update skipped')

    
    def sync_course_members(self, alias, user, group):
        """
        This method takes user input and executes a gam to cmd to sync users to a Google Classroom class

        `@param string` alias -> expected format subject-grade-year or class id, usually a string of numbers

        `@param string` user -> either teachers or students

        `@param string` group -> group email address of the group you wish to sync
        """
        from tools.cmd import run_cmd
        instruction = self.course + alias + " sync " + user + " group " + group
        print("Attempting to sync " + user + " from " + group)
        run_cmd(instruction)


    def sync_single_course_group(self):
        """
        This method is called if the user wants to sync a teacher or student group to single Google Classroom class.
        It asks for inputs and pipes the instructions to sync_course_members
        """
        from tools.functions import get_email
        def get_user(message):
            user = input(message)
            while user != 'teachers' and user != 'students':
                print('Incorrect input\n')
                user = input(message)
            return user

        alias = input('Enter the ID or alias of the course you wish to update\n')
        alias_message = 'Choose which group to sync:\nteachers | students\n'
        user = get_user(alias_message)
        email_message = 'Enter the assigned group email address to sync\n'
        group = get_email(email_message)
        
        self.sync_course_members(alias, user, group)